---
title: About
weight: 1
---

Hi, I'm James.

I am a musician,
coder,
and scrum master
and I like to make things.

With a background in Arts and Art Education,
I have been able to transpose my skills
and experiences in Arts management
and apply it in software delivery.

Creating safe
and inclusive spaces for people to come together
and create magical experiences for customers and users
has been a golden thread throughout my career.
Through my passion for people,
championing of innovation and diversity,
I have empowered individuals to create supportive,
resilient networks to achieve remarkable outcomes.
By fostering open, safe and collaborative spaces,
I have helped diverse teams thrive.

As an experienced Agile Delivery Manager,
I have a track record of
empowering teams to deliver exceptional outcomes
in dynamic and complex environments.
Through championing User Research and prioritising User needs,
I have delivered multiple safety critical,
high impact projects on time and to budget;
achieving improved customer
and user satisfaction
whilst maintaining regulatory compliance.
