# MakeJames

This repo is for the [makejames](https://makejames.com) site.
Using a bespoke [Hugo](https://gohugo.io/) Template,
this repo contains all the files necessary to build the site.

Previously hosted on GitHub,
this repository now builds and tags new images on merges to main.

## Roadmap

This is a list of upcoming features.

### v1.0

- [x] Site structure defined
- [x] Navigation and footer elements defined
- [x] Contact Form
- [ ] Accessibility testing on MR
- [ ] Pre-built inline icons
- [ ] Info panel shortcodes
- [ ] Footer social links
- [ ] Page content wrapped in article element tags
- [ ] Masonry style gallery
- [ ] Light and Dark mode toggle
- [ ] Search Bar
- [ ] Shortcodes to support a Resume
- [ ] Copyright notice
- [ ] Kofi link

## Future Releases

- Send emails from static site
- Image styling
- Video styling
- Music Player
- Lazy Load pictures
- Custom Logo
- Book a meeting / calendar integration
- Robots.txt file
- Related articles

### Blog

- Paginated Articles
- Tags & Categorisation
